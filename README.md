# THINK22 Lab 1203

IBM UrbanCode Deploy and Ansible better together - Think22 Lab 1203

## Setup instructions on your own environment

If you want to setup your demo/proof of concept environment on your own infrastructure, please have a look at the [Setup Environment instructions](Setup-Environment.md)

## The Lab Workbook

[Think22 1203 Lab Workbook - IBM UrbanCode Deploy and Ansible](1203_Lab_WorkBook.pdf)
