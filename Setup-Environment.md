# Setup the Environment

## Getting started

* First setup your ansible controler
* clone this repository
* Install ansible role requirements:
  * `ansible-galaxy install -r requirements.yml`
* edit hosts file, changing it to your setup
* Setup the target environment with Python3 and Ansible
  * run bootstrap setup
    * `ansible-playbook bootstrap.yml`
  * run Python3 setup
    * `ansible-playbook setup_python_310.yml`
  * run ansible setup
    * `ansible-playbook setup_ansible.yml`
* Setting up your UrbanCode Deploy Server environment
  * Please have a look at the following repository how to setup your UrbanCode Deploy Server
    * [UrbanCode Deploy Server Setup](#deploy-ucd-demo-environment-using-ansible)
  * To setup your agents either use the playbook provided in the original repo, or use the ones provided in this project
    * Setting up the local mysql database on the targets `ansible-playbook setup-base.yml`
    * Update the firewall rules (if needed) `ansible-playbook setup-firewall-rules.yml`
    * Finaly install the agents to the targets `ansible-playbook -i hosts -u root setup-ucd-agents.yml -e 'ucd_server_hostname=<name of the ucd server>'`
* Optional
  * You can use [Base JPetStore Demo Project](#deploy-jpetstore-demo-into-ucd) to setup the base Demo project (JPetStore Application) in UrbanCode Deploy

### external resources used

* Python related
  * <https://github.com/gforcada/ansible-compile-python>
  * <https://www.python.org/downloads/release/python-3104/>

### Location of the Ansible Plugin

* [Ansible Plugin](https://github.com/UrbanCode/Ansible-Toolkit-UCD)
  * [Release V53](https://github.com/UrbanCode/Ansible-Toolkit-UCD/releases/download/53/Ansible-Toolkit-UCD-v53.0a78951.zip)

### Apache-Tomcat binaries

* The Apache-Tomcat binaries are downloadable from "https://archive.apache.org/dist/tomcat"
* The original JPetStore Demo artifacts used Apache-Tomcat 7.0.43
* Feel free to add additional Versions

#### Downlad and prepare Apache-Tomcat binaries for apache-tomcat component

```sh
cd /home/JPetStore/shared
mkdir apache-tomcat
cd apache-tomcat

mkdir 7.0.42
wget https://archive.apache.org/dist/tomcat/tomcat-7/v7.0.42/bin/apache-tomcat-7.0.42.tar.gz -P "7.0.42"

mkdir 7.0.99
wget https://archive.apache.org/dist/tomcat/tomcat-7/v7.0.99/bin/apache-tomcat-7.0.99.tar.gz -P "7.0.99"

mkdir 9.0.36
wget https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.36/bin/apache-tomcat-9.0.36.tar.gz -P "9.0.36"

mkdir 10.0.20
wget https://archive.apache.org/dist/tomcat/tomcat-10/v10.0.20/bin/apache-tomcat-10.0.20.tar.gz -P "10.0.20"
```

## based on following projects

* [Deploy UCD Demo environment using Ansible](https://github.com/osmanburucu-ibm/deploy-urbancode-deploy)
* [Deploy JPetStore demo into UCD](https://github.com/osmanburucu-ibm/deploy-jpetstore-demo)

## Setting up environment from scratch

### Installing Python3 on Red Hat (Centos, Fedora, etc.) systems

```sh
sudo dnf install wget yum-utils make gcc openssl-devel bzip2-devel libffi-devel zlib-devel
wget https://www.python.org/ftp/python/3.10.2/Python-3.10.2.tgz
tar xzf Python-3.10.2.tgz
cd Python-3.10.2/
sudo ./configure --with-system-ffi --with-computed-gotos --enable-loadable-sqlite-extensions
sudo make -j ${nproc}
sudo make altinstall
whereis python3.10
alternatives --list | grep -i python3
sudo alternatives --install /usr/bin/python3 python3 /usr/local/bin/python3.10 2
sudo alternatives --config python3
```

### Install PIP

```sh
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py --user
sudo /usr/bin/python3 -m pip install --upgrade pip
```

### Install Ansible using pip

```sh
pip install ansible
```

### Install Java and set JAVA_JOME

#### Setting the JAVA_HOME environment variable

* Based on Input from following sources
  * [set java home path](https://www.cyberciti.biz/faq/linux-unix-set-java_home-path-variable/)
  * [Stackoverflow question about java home](https://stackoverflow.com/questions/24641536/how-to-set-java-home-in-linux-for-all-users)
  * [set java home on mac](https://www.codejava.net/java-core/set-java-home-in-macos-linux)

* create  /etc/profile.d/java_home.sh and add lines

```sh
#! /bin/bash

# Set JDK installation directory according to selected Java
export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:/bin/java::")
```

#### set symlinks

* update symlink "/etc/alternatives/jre" to the path above
* symlink "/usr/lib/jvm/jre" to "/etc/alternatives/jre"

## Troubleshooting

* Take care of
  * Ansible Toolkit (UCD plugin) V53 does not work with Groovy V3 (f.e. used by UrbanCode Deploy 7.2.2). Install "older" Agents which use Groovy V2 (for example v7.1.2)
  * If MYSql was previously installed the ansible playbook for setting up MYSql fails.
    * [Remove MYSQL configuration](#remove-mysql-configuration)
  * If during deployment run of JPetStore deployment an error occurs at the DB component (for example Schema Version exists)
    * [drop the databases in the affected MYSql server](#drop-demo-database-and-recreate)

### Remove MYSQL configuration

* Either manually remove "/root/.my.cnf" and "/root/.mysql_history"
* or use playbook with this tasks:

```yml
  - name: check if .my.cnf exists
    become:yes
    stat: path=/root/.my.cnf
    register: mycnf.stat

  - name: Move .my.cnf to bak
    command: mv /root/.my.cnf /root/.my.cnf.BAK
    when: mycnf.stat.exists
```

### Drop demo database and recreate

* change "<env>" to the affected database (dev, sit, uat)

```sh
mysql -u root -e "DROP DATABASE jpetstore_<env>"
mysql -u root -e "CREATE DATABASE jpetstore_<env>"
mysql -u root -e "grant all privileges on jpetstore_<env>.* to 'jpetstore'@'localhost'"
```
